# nml - Examples

this folder has a bunch of examples of what NML can do.

## Examples

- [Basic](Basic.nml) Is an basic example of the markup in nml.
- [Identation](Indetation.nml) Is an example of how to write nml with tabs instead of brackets (i.e. python style)
- [Styles](Styles.nml) Is an example of how to style elements in nml.
- [One line elements](OneLine.nml) Is an example of how to write unreadable code but conserve lines.
- [External styles](ExternalStyles/) Is an example on how to move styles to a different file.
- [External Content](ExternalContent/) Is an example on how to have content in different files.
- [Themes](Themes/) Is an example on how to use bootstrap to style your application

## TODO
